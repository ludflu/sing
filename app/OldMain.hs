module OldMain where

import Euterpea
import Scales

import System.Console.GetOpt
import Control.Monad
import System.IO
import System.Exit
import System.Environment

data Mood = Major | Minor

data Options = Options  { optVerbose    :: Bool
                        , mood :: Mood
                        , optInput      :: IO String
                        , optOutput     :: String -> IO ()
                        }

startOptions :: Options
startOptions = Options  { optVerbose    = False
                        , optInput      = getContents
                        , mood = Major
                        , optOutput     = putStr
                        }

options :: [ OptDescr (Options -> IO Options) ]
options =
    [ Option "i" ["input"]
        (ReqArg
            (\arg opt -> return opt { optInput = readFile arg })
            "FILE")
        "Input file"

    , Option "o" ["output"]
        (ReqArg
            (\arg opt -> return opt { optOutput = writeFile arg })
            "FILE")
        "Output file"

    , Option "s" ["string"]
        (ReqArg
            (\arg opt -> return opt { optInput = return arg })
            "FILE")
        "Input string"

    , Option "m" ["mood"]
        (ReqArg
            (\arg opt -> return opt { optInput = return arg })
            "FILE")
        "major or minor"

    , Option "v" ["verbose"]
        (NoArg
            (\opt -> return opt { optVerbose = True }))
        "Enable verbose messages"

    , Option "V" ["version"]
        (NoArg
            (\_ -> do
                hPutStrLn stderr "Version 0.01"
                exitWith ExitSuccess))
        "Print version"

    , Option "h" ["help"]
        (NoArg
            (\_ -> do
    	        prg <- getProgName
                hPutStrLn stderr (usageInfo prg options)
                exitWith ExitSuccess))
        "Show help"
    ]

mc= [
    1
    ,2
    ,3
    ,2
    ,3
    ,4
    ,5
    ,4
    ,3
    ,2
    ,2
    ,3
    ,4
    ,3
    ,2
    ,1
    ]

md = [1,2,3,1,2,5,6,7,6,7,6,5,6,7,2,1]

indexer :: [a] -> [Int] -> [a]
indexer notes indx =
   map (\x -> notes !! (x-1)) indx

m = indexer (cmajor qn) mc

main :: IO ()
main = do
    args <- getArgs

    -- Parse options, getting a list of option actions
    let (actions, nonOptions, errors) = getOpt RequireOrder options args

    -- Here we thread startOptions through all supplied option actions
    opts <- foldl (>>=) (return startOptions) actions

    let Options { optVerbose = verbose
                , optInput = input
                , optOutput = output   } = opts

    when verbose (hPutStrLn stderr "Hello!")

    --input >>= output
    play $ line $ indexer (aminor qn) md


--main :: IO ()
amain = do
          play $ line $ indexer (aminor qn) md


          --play $ line cantusFirmus
          --play $ line counterpoint
          --play $ line cantusFirmus :=: line counterpoint
