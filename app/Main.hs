{-# LANGUAGE OverloadedStrings #-}

module Main where

import Euterpea
import Scales (makeScale, makeMajorScale, makeMinorScale, majorPentatonic, minorPentatonic)

import Options.Applicative
import Data.Semigroup ((<>))
import Data.List.Split
import Text.Read
import Data.Char (toUpper)

data SingFlags = SingFlags
  { keynote    :: String
  , major      :: Bool
  , minor      :: Bool
  , pentatonic :: Bool
  }

singFlags :: Parser SingFlags
singFlags = SingFlags
      <$> strOption
          ( long "keynote"
          <> short 'k'
         <> metavar "KEYNOTE"
         <> help "the key note" )
      <*> switch
          ( long "major"
         <> help "interpret as notes on a major scale" )
      <*> switch
          ( long "minor"
         <> help "interpret as notes on a minor scale" )
      <*> switch
          ( long "pentatonic"
         <> help "interpret as notes on a pentatonic scale" )
    
main :: IO ()
main = run =<< execParser opts
  where
    opts = info (singFlags <**> helper)
      ( fullDesc
     <> progDesc "renders lines of comma delimited numbers as musical notes"
     <> header "sing - a midi music rendering" )

readInt :: String -> Maybe Int
readInt = readMaybe

parseNotes :: String -> Maybe [Int]
parseNotes ns = let tokens = splitOn "," ns
                 in mapM readInt tokens

up :: String -> String 
up = map toUpper

getKey :: String -> Maybe PitchClass
getKey k = readMaybe $ k

oct :: Int -> Int -> (Int,Int)
oct divisor dividend = let remainder = (dividend)  `mod` (divisor)
                           quotient = (dividend) `div` (divisor)
                        in remainder, quotient

octivize :: [Int] -> Int -> [(Int,Int)]
octivize indxs scaleSize = let aindxs = map (\x -> x - 1) indxs
                            in map (oct scaleSize) aindxs

raiseOctave :: Music Pitch -> Int -> Music Pitch
raiseOctave note octaves = shiftPitches (octaves * 12) note

-- calculate the index modulo the number of notes in the scale
-- when the index exceeds the number of notes, raise the note by the appropriate number of octaves
indexer :: [Music Pitch] -> [Int] -> [Music Pitch]
indexer scale indxs = let scaleSize = length scale
                          indxOctaves = octivize indxs scaleSize
                          indx = map fst indxOctaves
                          octaves = map snd indxOctaves
                          notes = map (\x -> scale !! x) indx
                          raise (p,i) = raiseOctave p i
                          toRaise = zip notes octaves
                       in map raise toRaise

playIt :: [Music Pitch] -> [Int] -> IO ()
playIt scale ns = do play $ line $ indexer scale ns

isMajor :: SingFlags -> Mode
isMajor flags = case (major flags, minor flags) of
  (True,True) -> Major
  (True,False) -> Major
  (False,False) -> Minor
  (False,True) -> Minor

getScale :: SingFlags -> Rational -> [Music Pitch]
getScale flags d = let key = getKey (keynote flags)
                       pent = pentatonic flags
                   in case key of
                        Just p -> case (isMajor flags) of
                                    Major -> if (pentatonic flags)
                                             then makeScale majorPentatonic (p,4) d
                                             else makeMajorScale (p,4) d
                                    Minor -> if (pentatonic flags) 
                                             then makeScale minorPentatonic (p,4) d
                                             else makeMinorScale (p,4) d
                        Nothing -> makeMajorScale (C,4) d


run :: SingFlags -> IO ()
run flags = do input <- getContents
               let ls = lines input
                   scale = getScale flags qn
                   stanzas = map parseNotes ls
               mapM_ (mapM (playIt scale)) stanzas